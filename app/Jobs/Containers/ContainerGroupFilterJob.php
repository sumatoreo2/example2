<?php
namespace App\Jobs\Containers;

use App\Models\Estimation\{AnalogFactorValue,
    ContainerGroupAnalog,
    ContainerGroupEvaluated,
    ContainerGroup,
    EvaluatedFactorValue};
use Illuminate\Bus\Queueable;
use Illuminate\Queue\{SerializesModels, InteractsWithQueue};
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class ContainerGroupFilterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Лимит на количество объектов для одновременной обработки
     */
    const LIMIT = 25000;

    /**
     * @var ContainerGroup
     */
    protected $containerGroup;

    /**
     * Create a new job instance.
     *
     * @param ContainerGroup $containerGroup
     * @throws \Exception
     */
    public function __construct(ContainerGroup $containerGroup)
    {
        $this->containerGroup = $containerGroup;

        if ($this->containerGroup->isArchiveItem()) {
            throw new \Exception('Группа архивная, разгруппировка запрещена!');
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->containerGroup->container_group_id) {
            if (!in_array($this->containerGroup->value['type'], ['range', 'array', 'catalog'])
                || !is_array($this->containerGroup->value['data'])) {
                throw new \InvalidArgumentException('Value filter is not valid');
            }

            $this->cleanEvaluatedGroup();
            $this->cleanAnalogGroup();
            $this->subDistributeEvaluated();
            $this->subDistributeAnalogs();
        } else {
            ContainerGroupEvaluated::where('container_group_id', $this->containerGroup->id)
                ->delete();
            ContainerGroupAnalog::where('container_group_id', $this->containerGroup->id)
                ->delete();
            $values = $this->containerGroup->value['data'];

            $this->evaluatedDistribute($values);
            $this->analogDistribute($values);
        }
    }

    /**
     * Разгруппировка аналогов
     *
     * @param array $values
     */
    protected function analogDistribute(array $values) {
        $factorId = $this->containerGroup->factor_id;
        $groupId = $this->containerGroup->id;

        $count = AnalogFactorValue::where('factor_id', $factorId)
            ->whereIn('value', $values)
            ->orderBy('analog_id', 'asc')
            ->count();

        $values = $this->getSegmentValues($values);

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $sql = "
                SELECT {$groupId}::integer as container_group_id, analog_id
                FROM estimation.analog_factor_values
                WHERE factor_id = :factor AND value IN ($values)
                ORDER BY analog_id ASC
                LIMIT :limit OFFSET :offset
            ";

            $data = DB::select($sql, [
                'factor' => $factorId,
                'limit' => self::LIMIT,
                'offset' => $offset
            ]);

            if (!empty($data)) {
                $data = $this->getGroupCollectValues($data, function ($item) {
                    return '(' . $item->container_group_id . ',' . $item->analog_id . ')';
                });

                $sql = "
                    INSERT INTO estimation.container_group_analogs (container_group_id, analog_id)
                    VALUES $data;
                ";

                DB::insert($sql);
            }
        }
    }

    /**
     * Разгруппировка объектов оценки
     *
     * @param array $values
     */
    protected function evaluatedDistribute(array $values) {
        $factorId = $this->containerGroup->factor_id;
        $groupId = $this->containerGroup->id;

        $count = EvaluatedFactorValue::where('factor_id', $factorId)
            ->whereExists(function ($query) {
                $query->select('main.evaluated_objects.id')
                    ->from('main.evaluated_objects')
                    ->whereRaw('main.evaluated_objects.id = evaluated_factor_values.evaluated_id
                        and main.evaluated_objects.actual_status = true');
            })
            ->whereIn('value', $values)
            ->orderBy('evaluated_id', 'asc')
            ->count();

        $values = $this->getSegmentValues($values);

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $sql = "
                SELECT {$groupId}::integer as container_group_id,
                       efv.evaluated_id as evaluated_object_id
                  FROM estimation.evaluated_factor_values efv
                 WHERE exists(
                    select null
                      from main.evaluated_objects eo
                     where eo.id = efv.evaluated_id
                       and eo.actual_status = true
                ) and efv.factor_id = :factor AND efv.value IN ($values)
                ORDER BY efv.evaluated_id ASC
                LIMIT :limit OFFSET :offset
            ";

            $data = DB::select($sql, [
                'factor' => $factorId,
                'limit' => self::LIMIT,
                'offset' => $offset
            ]);

            if (!empty($data)) {
                $data = $this->getGroupCollectValues($data, function ($item) {
                    return '(' . $item->container_group_id . ',' . $item->evaluated_object_id . ')';
                });

                $sql = "
                    INSERT INTO estimation.container_group_evaluated_objects (container_group_id, evaluated_object_id)
                    VALUES $data;
                ";

                DB::insert($sql);
            }
        }
    }

    /**
     * Разгруппировка подгруппы объектов оценки
     */
    protected function subDistributeEvaluated()
    {
        $factorId = $this->containerGroup->factor_id;
        $groupId = $this->containerGroup->id;
        $parentId = $this->containerGroup->container_group_id;

        $count = ContainerGroupEvaluated::where('container_group_id', $parentId)
            ->join('estimation.evaluated_factor_values', function ($join) use ($factorId) {
                $join->on('evaluated_id', '=', 'evaluated_object_id')->where('factor_id', $factorId);
            })->count();

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $data = ContainerGroupEvaluated::where('container_group_id', $parentId)
                ->join('estimation.evaluated_factor_values', function ($join) use ($factorId) {
                    $join->on('evaluated_id', '=', 'evaluated_object_id')->where('factor_id', $factorId);
                })
                ->orderBy('evaluated_object_id', 'asc')
                ->selectRaw('evaluated_object_id, value')
                ->limit(self::LIMIT);

            $ids = $this->filterByValues($data)
                ->pluck('evaluated_object_id')
                ->toArray();

            $this->updateContainerEvaluated($groupId, $parentId, $ids);
        }
    }

    /**
     * Разгруппировка подгруппы аналогов
     */
    protected function subDistributeAnalogs()
    {
        $factorId = $this->containerGroup->factor_id;
        $groupId = $this->containerGroup->id;
        $parentId = $this->containerGroup->container_group_id;

        $count = ContainerGroupAnalog::where('container_group_id', $parentId)
            ->join('estimation.analog_factor_values', function ($join) use ($factorId) {
                $join->on('estimation.analog_factor_values.analog_id', '=', 'estimation.container_group_analogs.analog_id')
                    ->where('factor_id', $factorId);
            })
            ->count();

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $data = ContainerGroupAnalog::where('container_group_id', $parentId)
                ->join('estimation.analog_factor_values', function ($join) use ($factorId) {
                    $join->on('estimation.analog_factor_values.analog_id', '=', 'estimation.container_group_analogs.analog_id')
                        ->where('factor_id', $factorId);
                })
                ->orderBy('analog_id', 'asc')
                ->selectRaw('container_group_analogs.analog_id, value')
                ->limit(self::LIMIT);

            $ids = $this->filterByValues($data)
                ->pluck('analog_id')
                ->toArray();

            $this->updateContainerAnalog($groupId, $parentId, $ids);
        }
    }

    /**
     * Фильтрует объекты по заданным параметрам
     *
     * @param $rows
     * @return array
     */
    protected function filterByValues($rows)
    {
        $data = $this->containerGroup->value['data'];
        $type = $this->containerGroup->value['type'];

        $filtered = [];

        if ($type === 'range') {
            list ($from, $to) = $data;

            if (is_null($to)) {
                $filtered = $rows->whereRaw('value::float >= ?', [$from]);
            } else {
                $filtered = $rows->whereRaw('value::float > ? and value::float <= ?', [(float)$from, (float)$to]);
            }
        } elseif (in_array($type, ['array', 'catalog'])) {
            $filtered = $rows->whereIn('value', $data);
        }

        return $filtered;
    }

    /**
     * Получить массив value в строке
     *
     * @param array $values
     * @return string
     */
    protected function getSegmentValues(array $values) : string {
        $values = array_map(function ($item) {
            return '\'' . $item .'\'';
        }, $values);
        $values = implode(',', $values);

        return $values;
    }

    /**
     * Получить сформированную строку данных
     * (группа, ид_объекта)
     *
     * @param $data
     * @param $callback
     * @return string
     */
    private function getGroupCollectValues($data, $callback) : string {
        $data = collect($data)->map($callback)->toArray();
        $data = implode(',', $data);

        return $data;
    }

    /**
     * Очистка подгруппы с перемещение объектов оценки в родительскую группу
     */
    private function cleanEvaluatedGroup() : void {
        $groupId = $this->containerGroup->id;
        $parentId = $this->containerGroup->container_group_id;

        $count = ContainerGroupEvaluated::where('container_group_id', $groupId)
            ->count();

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $ids = ContainerGroupEvaluated::where('container_group_id', $groupId)
                ->orderBy('evaluated_object_id', 'asc')
                ->limit(self::LIMIT)
                ->get()
                ->pluck('evaluated_object_id')
                ->toArray();

            $this->updateContainerEvaluated($parentId, $groupId, $ids);
        }
    }

    /**
     * Очистка подгруппы с перемещением аналогов в родительскую группу
     */
    private function cleanAnalogGroup() : void {
        $groupId = $this->containerGroup->id;
        $parentId = $this->containerGroup->container_group_id;

        $count = ContainerGroupAnalog::where('container_group_id', $groupId)
            ->count();

        for ($offset = 0; $offset <= $count; $offset += self::LIMIT) {
            $ids = ContainerGroupAnalog::where('container_group_id', $groupId)
                ->orderBy('analog_id', 'asc')
                ->selectRaw('analog_id')
                ->limit(self::LIMIT)
                ->get(['analog_id'])
                ->pluck('analog_id')
                ->toArray();

            $this->updateContainerAnalog($parentId, $groupId, $ids);
        }
    }

    /**
     * Обновление принадлежности объектов оценки к группе
     *
     * @param int $groupIn в какоую группу перепещать
     * @param int $groupOut из какой группы переместить
     * @param array $ids
     */
    private function updateContainerEvaluated(int $groupIn, int $groupOut, array $ids) : void {
        if (empty($ids)) return;

        $data = array_map(function ($id) use ($groupIn) {
            return "($groupIn, $id)";
        }, $ids);
        $values = implode(',', $data);

        $sql = "
                    UPDATE estimation.container_group_evaluated_objects AS t
                    SET container_group_id = c.container_group_id
                    FROM (
                        VALUES $values
                         ) AS c(container_group_id, evaluated_object_id)
                    WHERE c.evaluated_object_id = t.evaluated_object_id
                      AND t.container_group_id = ?;
                      ";

        DB::update($sql, [$groupOut]);
    }

    /**
     * Обновление принадлежности аналогов к группе
     *
     * @param int $groupIn в какую группу перемещать
     * @param int $groupOut из какой группы переместить
     * @param array $ids
     */
    private function updateContainerAnalog(int $groupIn, int $groupOut, array $ids) {
        if (empty($ids)) return;

        $data = array_map(function ($id) use ($groupIn) {
            return "($groupIn, $id)";
        }, $ids);
        $values = implode(',', $data);

        $sql = "
                    UPDATE estimation.container_group_analogs AS t
                    SET container_group_id = c.container_group_id
                    FROM (
                        VALUES $values
                         ) AS c(container_group_id, analog_id)
                    WHERE c.analog_id = t.analog_id
                      AND t.container_group_id = ?;
                      ";

        DB::update($sql, [$groupOut]);
    }
}
