<?php

namespace App\Jobs\Containers;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Classes\FactorValues\{ContainerAnalogValues, ContainerGroupAnalogValues};

class ExportAnalogFactorsJob extends AbstractExportFactors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const TYPE = 'a';
    /**
     * @var array
     */
    protected $params;
    /**
     * @var bool
     */
    protected $isGroup;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     */
    public function __construct(array $keys = [])
    {
        $keys['chunkSize'] = 1000;

        parent::__construct($keys);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->instanceSourceEntity();

            Storage::disk('public')->put($this->getOutputFile(), '');

            $this->write();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Storage::disk('public')->delete($this->getOutputFile());
        }
    }

    /**
     * Создает экземпляр класса, который будет использоваться,
     * как источник данных
     *
     * @return void
     */
    protected function instanceSourceEntity() : void
    {
        if ($this->isGroup) {
            $this->sourceEntity = new ContainerGroupAnalogValues($this->params['id']);
        } else {
            $this->sourceEntity = new ContainerAnalogValues($this->params['id']);
        }
    }
}
