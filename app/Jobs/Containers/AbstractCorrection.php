<?php


namespace App\Jobs\Containers;


use App\Jobs\AbstractJob;
use App\Models\Estimation\ContainerGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

abstract class AbstractCorrection extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Лимит на одновременную обработку данных
     */
    const LIMIT = 20000;

    /**
     * @var ContainerGroup
     */
    protected $group;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     * @throws \Exception
     */
    public function __construct(array $keys = [])
    {
        $this->group = ContainerGroup::find($keys['id']);

        if ($this->group->isArchiveItem()) {
            throw new \Exception('Группа архивная, расчет корректировок запрещен!');
        }

        parent::__construct($keys);
    }

    protected function setName(): void
    {
        $this->name = 'Абстрактная корректировка: ' . $this->group->name;
    }

    protected function setTemplate(): void
    {
        $this->template = 'task.correction.group.{id}';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        bcscale(config('bcmath.bcscale.correction'));

        try {
            $this->recursiveCorrection($this->group);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * Фильтрует объекты по заданным параметрам
     *
     * @param Builder $builder
     * @param array $correction
     * @return Builder
     */
    protected function filterByValues(Builder $builder, array $correction) : Builder
    {
        $type = $correction['factor_type'];
        $data = $correction['data'];

        $filtered = clone $builder;

        if ($type === 'range') {
            list ($from, $to) = $data;

            if (is_null($to)) {
                $filtered = $filtered->whereRaw('value::float >= ?', [$from]);
            } else {
                $filtered = $filtered->whereRaw('value::float > ? and value::float <= ?', [(float)$from, (float)$to]);
            }
        } elseif (in_array($type, ['array', 'catalog'])) {
            $filtered = $filtered->whereIn('value', $data);
        }

        return $filtered;
    }

    /**
     * Получить корректировку для текущей группы.
     * Происходит фильтрация факторов родительской и текущей корерктировок,
     * при совпадении факторов родительской и текущей, родительская корректировка отфильтровывается
     *
     * @param HasMany $correctionsCollection
     * @param array $corrections
     * @return array
     */
    protected function takeCorrections(HasMany $correctionsCollection, array $corrections = []) : array
    {
        $currentCorrections = $correctionsCollection
            ->with(['factor' => function($query) {
                $query->select('factor_type');
            }])
            ->get(['name', 'type', 'factor_type', 'factor_id', 'data', 'value', 'skip'])
            ->toArray();

        if (!empty($corrections)) {
            foreach ($currentCorrections as  $currentCorrection) {
                $corrections = array_filter($corrections, function ($correction) use ($currentCorrection) {
                    return $correction['factor_id'] !== $currentCorrection['factor_id'];
                });
            };

            $currentCorrections = array_merge($currentCorrections, $corrections);
        }

        return $currentCorrections;
    }

    /**
     * Логика для записи в бд объектов с подсчитанной корректиировкой
     *
     * @param ContainerGroup $containerGroup
     * @param array $adjustedObjects
     * @param string $sql
     */
    protected function recordCalculated(ContainerGroup $containerGroup, array $adjustedObjects, string $sql): void {
        if (filled($adjustedObjects)) {
            $containerId = $containerGroup->container_id;
            $keys = array_keys($adjustedObjects);
            $values = array_values($adjustedObjects);

            $items = array_map(function ($id, $cost) use ($containerId) {
                return "($containerId, $id, $cost)";
            }, $keys, $values);

            $items = implode(',', $items);
            $sql = str_replace(':items', $items, $sql);

            DB::transaction(function () use ($sql) {
                DB::insert($sql);
            });
        }
    }

    /**
     * Метод рекурсивного прохода по группам с аккумуляцией корректировок и одновременным расчетом
     *
     * @param ContainerGroup $containerGroup
     * @param array $corrections
     */
    abstract protected function recursiveCorrection(ContainerGroup $containerGroup, $corrections = []) : void;

    /**
     * Применение корректировок
     *
     * @param ContainerGroup $containerGroup
     * @param array $currentCorrections
     */
    abstract protected function applyCorrections(ContainerGroup $containerGroup, array $currentCorrections) : void;

    /**
     * Расчет УПКС
     * с учетом корректировки
     *
     * @param float $cost
     * @param array $correction
     * @return float
     */
    abstract protected function calculate(float $cost, array $correction) : float;
}
