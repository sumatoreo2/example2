<?php

namespace App\Jobs\Containers;

use App\Models\Estimation\{ContainerGroup,
    ContainerGroupCorrection,
    ContainerGroupEvaluated};
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Обратная коррекция
 *
 * Class ReverseCorrectionJob
 * @package App\Jobs\Containers
 */
class ReverseCorrectionJob extends CorrectionJob
{
    /**
     * Установка имени
     */
    protected function setName(): void
    {
        $this->name = 'Применение обратной корректировки: ' . $this->group->name;
    }

    /**
     * Установка шаблона
     */
    protected function setTemplate(): void
    {
        $this->template = 'task.reverse.correction.{id}';
    }

    /**
     * Метод рекурсивного прохода по группам с аккумуляцией корректировок и одновременным расчетом
     *
     * @param ContainerGroup $containerGroup
     * @param array $corrections
     */
    protected function recursiveCorrection(ContainerGroup $containerGroup, $corrections = []): void
    {
        $correctionCollection = $containerGroup->corrections()
            ->where('skip', false);
        $currentCorrections = $this->takeCorrections($correctionCollection, $corrections);

        array_multisort(array_column($currentCorrections, 'type'), SORT_ASC, $currentCorrections);

        if ($containerGroup->calculation_method !== ContainerGroup::CALCULATION_METHOD_CA) {
            $this->applyCorrections($containerGroup, $currentCorrections);
        }

        $containerGroup->groups()->each(function (ContainerGroup $item) use ($currentCorrections) {
            $this->recursiveCorrection($item, $currentCorrections);
        });
    }

    /**
     * Применение корректировок
     * Сначала происходит проверка на существование аналогов в группе
     * После производится корректировка
     *
     * @param ContainerGroup $containerGroup
     * @param array $currentCorrections
     */
    protected function applyCorrections(ContainerGroup $containerGroup, array $currentCorrections): void
    {
        if ($containerGroup->containerGroupEvaluated()->exists()) {
            $evaluatedObjects = [];

            foreach ($currentCorrections as $correction) {
                $builder = ContainerGroupEvaluated::where('container_group_id', $containerGroup->id)
                    ->join('estimation.evaluated_factor_values', function ($join) use ($correction) {
                        $join->on('evaluated_id', '=', 'evaluated_object_id')->where('factor_id', $correction['factor_id']);
                    })
                    ->join('estimation.container_cadastral_costs', function ($join) use ($containerGroup) {
                        $join->on('estimation.container_cadastral_costs.evaluated_object_id', '=', 'estimation.container_group_evaluated_objects.evaluated_object_id')
                            ->where('estimation.container_cadastral_costs.container_id', $containerGroup->container_id);
                    })
                    ->orderBy('estimation.container_group_evaluated_objects.evaluated_object_id', 'asc')
                    ->select('value', 'estimation.container_group_evaluated_objects.evaluated_object_id', 'estimation.container_cadastral_costs.before_correction as cost');

                $filtered = $this->filterByValues($builder, $correction);

                if ($filtered->exists()) {
                    $filtered->chunk(self::LIMIT, function (Collection $items) use ($correction, &$evaluatedObjects, $containerGroup) {
                        $items = $items->toArray();

                        foreach ($items as $item) {
                            $id = $item['evaluated_object_id'];
                            $cost = isset($evaluatedObjects[$id]) ? $evaluatedObjects[$id] : $item['cost'];
                            $cost = $this->calculate($cost, $correction);

                            $evaluatedObjects[$id] = $cost;
                        }
                    });
                }
            }

            $sql = '
                INSERT INTO estimation.container_cadastral_costs (container_id, evaluated_object_id, cost)
                VALUES :items
                ON CONFLICT (container_id, evaluated_object_id) DO UPDATE SET cost=EXCLUDED.cost;
            ';
            $this->recordCalculated($containerGroup, $evaluatedObjects, $sql);
        }
    }

    /**
     * Расчет цены
     * с учетом корректировки
     * Значения при абсолютной корректировке инвертируют знак
     *
     * @param float $cost
     * @param array $correction
     * @return float
     */
    protected function calculate(float $cost, array $correction): float
    {
        $type = $correction['type'];
        $value = floatval($correction['value']);

        if ($type === ContainerGroupCorrection::CORRECTION_TYPE_RELATIVE) {
            $cost = $cost > 0 ? bcdiv($cost, $value) : 0;
        } else if ($type === ContainerGroupCorrection::CORRECTION_TYPE_ABSOLUTE) {
            $value = -1 * $value;
            $cost = bcadd($cost, $value);
        }

        return round($cost ?? 0, 2);
    }
}
