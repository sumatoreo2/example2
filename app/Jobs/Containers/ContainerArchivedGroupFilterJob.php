<?php

namespace App\Jobs\Containers;

use Illuminate\Support\Facades\DB;

/**
  * Распределение объектов для сегментов архивных контейнеров
  */
class ContainerArchivedGroupFilterJob extends ContainerGroupFilterJob
{
    /**
     * @param array $values
     */
    protected function evaluatedDistribute(array $values)
    {
        $values = $this->getSegmentValues($values);

        $sql = <<<SQL
            INSERT INTO estimation.container_group_evaluated_objects(container_group_id, evaluated_object_id)
            select :container_group_id as container_group_id,
                   eo.id as evaluated_object_id
              from main.v_evaluated_objects eo
              join estimation.evaluated_factor_values efv 
                on efv.evaluated_id = eo.id 
               and efv.factor_id = :factor_id 
               and efv.value in ($values)
             where eo.actual_status = true
               and eo.archived = false
SQL;
        DB::insert($sql, [
            'container_group_id' => $this->containerGroup->id,
            'factor_id' => $this->containerGroup->factor_id
        ]);
    }

    /**
     * @param array $values
     */
    protected function analogDistribute(array $values)
    {

    }
}
