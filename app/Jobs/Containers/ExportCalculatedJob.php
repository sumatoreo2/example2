<?php

namespace App\Jobs\Containers;

use App\Classes\Reports\Report;
use App\Classes\Reports\ReportFactory;
use App\Jobs\AbstractJob;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Estimation\{Container};

/**
 * Экспорт результатов расчета
 *  кадастровой стоимости по текущему контейнеру
 *  по форме 'Результаты определения КС вер.2 в Excel'
 *
 * CFSCE-350
 *
 **/
class ExportCalculatedJob extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Container
     */
    private $container;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     */
    public function __construct(array $keys = [])
    {
        $this->container = Container::find($keys['id']);


        parent::__construct($keys);
    }

    protected function setName(): void
    {
        $this->name = 'Результаты определения КС вер.2 в Excel: ' . $this->container->name;
    }

    protected function setTemplate(): void
    {
        $this->template = 'task.export.excel.{id}';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $factory = ReportFactory::factoryContainer($this->container, Report::CODE_CONTAINER_RESULT_CC_V2);

        $factory->exportResultToExcel();
    }
}
