<?php

namespace App\Jobs\Containers;

use App\Classes\Reports\Exports\Container\ExportZipContainerCalculated;
use App\Jobs\AbstractJob;
use App\Models\Estimation\Container;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Массовая выгрузка результатов расчета в ZIP архив
 *
 */
class ExportZipCalculatedJob extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Container
     */
    private $container;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     */
    public function __construct(array $keys = [])
    {
        $this->container = Container::find($keys['id']);

        parent::__construct($keys);
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $zip = new ExportZipContainerCalculated($this->container);

        $zip->generate();
    }

    /**
     * Нужно реализовать метод для установки имени для отображения на фронте
     */
    protected function setName(): void
    {
        $this->name = 'Массовая выгрузка результатов расчета в ZIP: ' . $this->container->name;
    }

    /**
     * Нужно реализовать метод для установки шаблона идентификатора
     */
    protected function setTemplate(): void
    {
        $this->template = 'task.export.zip.calculate.{id}';
    }
}
