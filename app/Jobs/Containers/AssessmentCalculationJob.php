<?php

namespace App\Jobs\Containers;

use App\Classes\CalculationMethods\CalculationFactory;
use App\Jobs\AbstractJob;
use App\Models\Estimation\{Container, ContainerGroup};
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AssessmentCalculationJob extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Получить сегменты контейнера
     * @var string
     */
    private $getSegments = '
                SELECT id
                FROM estimation.container_groups cg
                WHERE cg.container_id = :ci AND cg.container_group_id isnull
                ORDER BY cg.id ASC
            ';

    /**
     * Получить все группы для
     * @var string
     */
    private $getCalculationGroups = '
            WITH RECURSIVE nodes(id, container_group_id, calculation_method) AS (
                SELECT s1.id, s1.container_group_id, s1.calculation_method
                FROM estimation.container_groups s1
                WHERE container_group_id = :cgi
                   OR id = :cgi
                UNION
                SELECT s2.id, s2.container_group_id, s2.calculation_method
                FROM estimation.container_groups s2,
                     nodes s1
                WHERE s2.container_group_id = s1.id
                  AND s1.container_group_id != s2.id
            )
            SELECT n.id, n.calculation_method, n.container_group_id
            FROM nodes n
            WHERE n.calculation_method = :cm
              AND n.id NOT IN (
                WITH RECURSIVE nodes(id, container_group_id, calculation_method) AS (
                    SELECT s1.id, s1.container_group_id, s1.calculation_method
                    FROM estimation.container_groups s1
                    WHERE container_group_id = :cgi
                       OR id = :cgi
                    UNION
                    SELECT s2.id, s2.container_group_id, s2.calculation_method
                    FROM estimation.container_groups s2,
                         nodes s1
                    WHERE s2.container_group_id = s1.id
                      AND s1.container_group_id != s2.id
                )
                SELECT n.container_group_id as id
                FROM nodes n
                WHERE n.calculation_method = :cm
                ORDER BY n.id ASC
            )
            ORDER BY n.id ASC
        ';

    /**
     * Create a new job instance.
     *
     * @param array $keys
     */
    public function __construct(array $keys = [])
    {
        parent::__construct($keys);
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $assessmentObject = $this->keys['object'];

        if ($assessmentObject->isArchiveItem()) {
            throw new \Exception('Раздел архивный, расчет не доступен!');
        }

        if ($assessmentObject instanceof ContainerGroup) {
            $this->calculate($assessmentObject->id);
        } elseif ($assessmentObject instanceof Container) {
            $segments = DB::select($this->getSegments, ['ci' => $assessmentObject->id]);
            $segments = array_column($segments, 'id');

            $calculationMethods = array_column(ContainerGroup::CALCULATION_METHOD, 'value');

            foreach ($segments as $segment) {
                foreach ($calculationMethods as $calculationMethod) {
                    $calculationGroups = DB::select($this->getCalculationGroups, ['cgi' => $segment, 'cm' => $calculationMethod]);

                    foreach ($calculationGroups as $calculationGroup) {
                        $this->calculate($calculationGroup->id);
                    }
                }
            }
        }
    }

    /**
     * @param int $groupId
     * @return void
     */
    protected function calculate(int $groupId)
    {
        try {
            $handler = CalculationFactory::factory($groupId);
            $handler->run();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            Log::debug($e->getTraceAsString());
        }
    }

    /**
     * Установка имени
     */
    protected function setName(): void
    {
        $assessmentObject = $this->keys['object'];

        if ($assessmentObject instanceof ContainerGroup) {
            $this->name = 'Расчет объектов оценки, группа: ' . $assessmentObject->name;
        } elseif ($assessmentObject instanceof Container) {
            $this->name = 'Расчет объектов оценки, контейнер: ' . $assessmentObject->name;
        }
    }

    /**
     * Шаблон иденификатора
     */
    protected function setTemplate(): void
    {
        $assessmentObject = $this->keys['object'];

        if ($assessmentObject instanceof ContainerGroup) {
            $this->template = 'task.assessment.group.{id}';
        } elseif ($assessmentObject instanceof Container) {
            $this->template = 'task.assessment.container.{id}';
        }
    }
}
