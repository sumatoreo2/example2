<?php

namespace App\Jobs\Containers;

use App\Jobs\AbstractJob;
use App\Models\Estimation\Container;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 *  Запуск процесса архивирования контейнера
 *
 * @property Container $container
 * @property bool $isArchived
 */
class ContainerArchivedJob extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Текущий контейнер
     *
     * @var Container
     */
    private $container;

    /**
     * Признак архивировани
     * true - setArchived
     * false - unSetArchived
     *
     * @var bool
     */
    private $isArchived;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     * @throws \Exception
     */
    public function __construct(array $keys = [])
    {
        $this->container = Container::find($keys['id']);
        $this->isArchived = $keys['isArchived'];

        if (!$this->container->isFullCalculationCostEvaluatedObjects()) {
            throw new \Exception('Контейнер полностью не рассчитан!');
        }

        parent::__construct($keys);
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->isArchived) {
            $this->container->setArchivedContainer();
        } else {
            $this->container->unSetArchivedContainer();
        }
    }

    /**
     * Нужно реализовать метод для установки имени для отображения на фронте
     */
    protected function setName(): void
    {
        $this->name = 'Архивирование контейнера: ' . $this->container->name;
    }

    /**
     * Нужно реализовать метод для установки шаблона идентификатора
     */
    protected function setTemplate(): void
    {
        $this->template = 'task.containers.archived.{id}';
    }
}
