<?php

namespace App\Jobs\Containers;

use App\Classes\FactorValues\FactorValuesGetterInterface;
use App\Jobs\AbstractJob;
use App\Models\Estimation\Container;
use App\Models\Estimation\ContainerFactor;
use App\Models\Estimation\ContainerGroup;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Illuminate\Support\Collection;

abstract class AbstractExportFactors extends AbstractJob
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @var bool
     */
    protected $isGroup;

    /**
     * @var Collection
     */
    protected $factors;

    /**
     * @var array
     */
    protected $mainColumns = ['id'];

    /**
     * @var FactorValuesGetterInterface
     */
    protected $sourceEntity;

    /**
     * @param array $keys
     */
    public function __construct(array $keys = [])
    {
        $this->params = $keys;
        $this->params['segment_type'] = ucfirst(strtolower($this->params['segment_type']));
        $this->isGroup = isset($keys['is_group']);
        $this->setFactors();

        parent::__construct($keys);
    }

    /**
     * Установить имя
     */
    protected function setName(): void
    {
        if (!$this->isGroup) {
            $container = Container::find($this->params['id']);
            $this->name = 'Экспорт значений контейнера: ' . $container->name;
        } else {
            $containerGroup = ContainerGroup::find($this->params['group_id']);
            $this->name = 'Экспорт значений группы: ' . $containerGroup->name;
        }
    }

    /**
     * Установить шаблон
     */
    protected function setTemplate(): void
    {
        if ($this->isGroup) {
            $this->template = 'task.export.group.{group_id}';
        } else {
            $this->template = 'task.export.container.{id}';
        }
    }

    /**
     * Возвращает путь и название файла
     *
     * @return string
     */
    public function getOutputFile()
    {
        $file = 'exports/factor-values/' . $this->params['id'] . "/" . static::TYPE . "_";

        if ($this->isGroup) {
            $file .= $this->params['group_id'] . '_';
        }

        $file .= Carbon::now()->format('Y_m_d_H_i_s') . '.' . Type::XLSX;

        return $file;
    }

    /**
     * Запись в файл
     *
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function write()
    {
        $writer = WriterEntityFactory::createWriter(Type::XLSX);
        $writer->openToFile(storage_path('app/public/' . $this->getOutputFile()));

        $columns = $this->columns();
        array_unshift($columns, ...$this->mainColumns);
        $row = WriterEntityFactory::createRowFromArray($columns);
        $writer->addRow($row);

        $this->pull(function ($items) use ($writer) {
            $writer->addRows($items);
        });

        $writer->close();
    }

    /**
     * Преобразовывает коллекцию факторов в массив
     *
     * @return array
     */
    public function columns()
    {
        return $this->factors->toArray();
    }

    /**
     * Достает из БД данные и записывает в Excel файл
     *
     * @param \Closure $closure
     */
    protected function pull(\Closure $closure)
    {
        $max = $this->params['chunkSize'];
        $total = $this->count();

        $pages = ceil($total / $max);

        for ($i = 1; $i < ($pages + 1); $i++) {
            $offset = (($i - 1)  * $max);
            $start = ($offset == 0 ? 0 : ($offset + 1));

            $rows = $this->values($start, $max);

            foreach ($rows as $key => &$row) {
                $row = (array) $row;
                $it = new \RecursiveIteratorIterator(new \RecursiveArrayIterator(json_decode($row['value'],true)));

                foreach ($this->factors as $factorId => $factorName) {
                    $row[$factorName] = '-';
                    foreach ($it as $id => $value) {
                        if ($id === $factorId) {
                            $row[$factorName] = $value;
                        }
                    }
                }

                unset($row['value']);
                $row = WriterEntityFactory::createRowFromArray($row);
            }

            $closure($rows);
        }
    }

    /**
     * Достает и устанавливает факторы контейнера
     *
     * @return void
     */
    protected function setFactors()
    {
        $this->factors = ContainerFactor::with('factor:id,name')
            ->where('container_id', $this->params['id'])
            ->get()
            ->pluck( 'factor.name', 'factor.id');
    }

    /**
     * Возвращает значения факторов
     *
     * @param int $offset
     * @param int $limit
     * @return array
     */
    protected function values(int $offset, int $limit)
    {
        $this->sourceEntity->setOffset($offset);
        $this->sourceEntity->setLimit($limit);

        return $this->sourceEntity->fetch();
    }

    /**
     * Возвращает общее количество значений факторов
     *
     * @return int
     */
    protected function count() : int
    {
        return $this->sourceEntity->getCount();
    }
}
