<?php

namespace App\Jobs\Containers;

use App\Jobs\AbstractJob;
use App\Models\Estimation\Container;
use App\Models\Estimation\ContainerGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

/**
 * Задача для повторной разгруппировки групп контейнера
 *
 * Class GroupDistributionJob
 * @package App\Jobs\Containers
 */
class GroupDistributionJob extends AbstractJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Container
     */
    private $container;

    /**
     * Create a new job instance.
     *
     * @param array $keys
     * @throws \Exception
     */
    public function __construct(array $keys = [])
    {
        $this->container = Container::find($keys['id']);

        if($this->container->isArchiveItem()) {
            throw new \Exception('Контейнер архивный, разгруппировка запрещена!');
        }

        parent::__construct($keys);
    }

    protected function setName(): void
    {
        $this->name = 'Разгруппировка контейнера: ' . $this->container->name;
    }

    protected function setTemplate(): void
    {
        $this->template = 'task.ungrouping.container.{id}';
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteAnalogs();
        $this->deleteEvaluatedObjects();

        $groups = $this->container->containerGroups()
            ->whereNull('container_group_id')
            ->get();

        foreach ($groups as $group) {
            $this->checkGroup($group);
        }
    }

    public function checkGroup(ContainerGroup $cg) {
        if (isset($cg->archivedGroup) && !isset($cg->container_group_id)) {
            ContainerArchivedGroupFilterJob::dispatchNow($cg);
        } else {
            ContainerGroupFilterJob::dispatchNow($cg);
        }

        $groups = $cg->groups;

        if (filled($groups)) {
            foreach ($groups as $group) {
                $this->checkGroup($group);
            }
        }
    }

    /**
     * Удаляем все значения аналогов из групп контейнера
     */
    private function deleteAnalogs() : void {
        $sql = "
            DELETE FROM estimation.container_group_analogs cga
                USING estimation.container_groups cg
            WHERE cg.id = cga.container_group_id
            AND cg.container_id = ?
        ";

        DB::delete($sql, [$this->container->id]);
    }

    /**
     * Удаляем все значения объектов оценки из групп контейнера
     */
    private function deleteEvaluatedObjects() : void {
        $sql = "
            DELETE FROM estimation.container_group_evaluated_objects cgeo
                USING estimation.container_groups cg
            WHERE cg.id = cgeo.container_group_id
            AND cg.container_id = ?
        ";

        DB::delete($sql, [$this->container->id]);
    }
}
