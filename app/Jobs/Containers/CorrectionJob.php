<?php

namespace App\Jobs\Containers;

use App\Models\Estimation\{ContainerGroup,
    ContainerGroupAnalog,
    ContainerGroupCorrection};
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

/**
 * Прямая корректировка
 *
 * Class CorrectionJob
 * @package App\Jobs\Containers
 */
class CorrectionJob extends AbstractCorrection
{
    protected function setName(): void
    {
        $this->name = 'Применение прямой корректировки: ' . $this->group->name;
    }

    protected function setTemplate(): void
    {
        $this->template = 'task.correction.group.{id}';
    }

    /**
     * Метод рекурсивного прохода по группам с аккумуляцией корректировок и одновременным расчетом
     *
     * @param ContainerGroup $containerGroup
     * @param array $corrections
     */
    protected function recursiveCorrection(ContainerGroup $containerGroup, $corrections = []) : void {
        $correctionCollection = $containerGroup->corrections();
        $currentCorrections = $this->takeCorrections($correctionCollection, $corrections);

        array_multisort(array_column($currentCorrections, 'type'), SORT_DESC, $currentCorrections);

        $this->applyCorrections($containerGroup, $currentCorrections);

        $containerGroup->groups()->each(function (ContainerGroup $item) use ($currentCorrections) {
            $this->recursiveCorrection($item, $currentCorrections);
        });
    }

    /**
     * Применение корректировок
     * Сначала происходит проверка на существование аналогов в группе
     * После производится корректировка
     *
     * @param ContainerGroup $containerGroup
     * @param array $currentCorrections
     */
    protected function applyCorrections(ContainerGroup $containerGroup, array $currentCorrections) : void {
        if ($containerGroup->containerGroupAnalogs()->exists()) {
            $analogs = [];

            $this->fillAnalogReferenceValues($containerGroup);

            foreach ($currentCorrections as $correction) {
                $builder = ContainerGroupAnalog::where('container_group_id', $containerGroup->id)
                    ->join('estimation.analog_factor_values', function ($join) use ($correction) {
                        $join->on('estimation.analog_factor_values.analog_id', '=', 'estimation.container_group_analogs.analog_id')
                            ->where('factor_id', $correction['factor_id']);
                    })
                    ->join('analogs.analogs', function ($join) {
                        $join->on('analogs.analogs.id', '=', 'container_group_analogs.analog_id');
                    })
                    ->select('value', 'container_group_analogs.analog_id', 'analogs.price')
                    ->orderBy('analog_id', 'asc');

                $filtered = $this->filterByValues($builder, $correction);

                if ($filtered->exists()) {
                    $filtered->chunk(self::LIMIT, function (Collection $items) use ($correction, &$analogs, $containerGroup) {
                        $items = $items->toArray();

                        foreach ($items as $item) {
                            $id = $item['analog_id'];
                            $cost = isset($analogs[$id]) ? $analogs[$id] : $item['price'];
                            $cost = $this->calculate($cost, $correction);

                            $analogs[$id] = $cost;
                        }
                    });
                }
            }

            $sql = '
                INSERT INTO estimation.analog_reference_values (container_id, analog_id, cost)
                VALUES :items
                ON CONFLICT (container_id, analog_id) DO UPDATE SET cost=EXCLUDED.cost;
            ';
            $this->recordCalculated($containerGroup, $analogs, $sql);
        }
    }

    /**
     * Заполняет и обновляет таблицу эталонной стоимости текущим значением цены
     *
     * @param ContainerGroup $containerGroup
     */
    private function fillAnalogReferenceValues(ContainerGroup $containerGroup) : void {
        $sql = '
        INSERT INTO estimation.analog_reference_values (container_id, analog_id, cost)
        SELECT cg.container_id, a.id AS analog_id, a.price AS cost
        FROM estimation.container_group_analogs cga
            JOIN analogs.analogs a ON a.id = cga.analog_id
            JOIN estimation.container_groups cg ON cg.id = cga.container_group_id
        WHERE cga.container_group_id = :group_id
        ON CONFLICT (container_id, analog_id) DO UPDATE SET cost=EXCLUDED.cost;
        ';

        DB::transaction(function () use ($sql, $containerGroup) {
            DB::insert($sql, ['group_id' => $containerGroup->id]);
        });
    }

    /**
     * Расчет цены
     * с учетом корректировки
     *
     * @param float $cost
     * @param array $correction
     * @return float
     */
    protected function calculate(float $cost, array $correction) : float
    {
        $type = $correction['type'];
        $value = floatval($correction['value']);

        if ($type === ContainerGroupCorrection::CORRECTION_TYPE_RELATIVE) {
            $cost = bcmul($cost, $value);
        } else if ($type === ContainerGroupCorrection::CORRECTION_TYPE_ABSOLUTE) {
            $cost = bcadd($cost, $value);
        }

        return round($cost ?? 0, 2);
    }
}
